
resource "aws_subnet" "subnet" {
  vpc_id                  = var.vpcId
  cidr_block              = var.cidr
  tags                    = var.tags
  availability_zone       = var.AZ
  map_public_ip_on_launch = var.publicIp
}
resource "aws_route_table_association" "a" {
  subnet_id               = aws_subnet.subnet.id
  route_table_id          = var.publicRtId
}
