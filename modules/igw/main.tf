resource "aws_internet_gateway" "igw" {
  vpc_id        = var.vpcId
  tags          = var.tags
}
