
variable "tags" {
  type              = map(string)
  description       = "tag for internet gateway of nginx"
  default           = {
    Name            = "InternetGateway"
    Owner           = "satender"
  }

}
variable "vpcId" {
  type              = string
  default           = ""
}

