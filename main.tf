locals {
  name                  = "nginx"
  purpose               = "web server"
  type                  = "nginx"
  owner                 = "satender sharma"
 
  all_public_subnetsId = [module.public_subnet["ap-southeast-2a"].subnetId, module.public_subnet["ap-southeast-2b"].subnetId]
}

module "vpc" {
  source                = "./modules/vpc"
  cidr                  = var.vpc_cidr
  tags                  = {
    Name                = "${local.name}-vpc"
    Owner               = local.owner
    Purpose             = local.purpose
  }
}

module "public_subnet" {
  source                = "./modules/subnet"
  vpcId                 = module.vpc.vpcId
  for_each              = var.cidr_public
  # cidr                  = "10.10.0.64/26"
  # AZ                    = "ap-southeast-2a"
  AZ                    = each.key
  cidr                  = each.value
  publicIp              = true
  publicRtId          = module.publicRT.igw-id
  tags = {
    Name                = "publicSubnet-${each.key}"
    Owner               = local.owner
    Purpose             = local.purpose
  }
}

module "igw" {
  source                = "./modules/igw"
  vpcId                 = module.vpc.vpcId
  tags                  = {
    Name                = "${local.name}-igw"
    Owner               = local.owner
    Purpose             = local.purpose
  }
}

module "publicRT" {
  source                = "./modules/route_table"
  vpcId                 = module.vpc.vpcId
  igw-id                = module.igw.igwId
  tags                  = {
    Name                = "${local.name}-publicRT"
    Owner               = local.owner
    Purpose             = local.purpose
  }
}

module "securityGroup" {
  source                = "./modules/Security_group"
  vpcId                 = module.vpc.vpcId

  tags                  = {
    Name                = "public-sg"
    Owner               = local.owner
    Purpose             = local.purpose
  }
}

module "Public_instance" {
  source                = "./modules/EC2_instance"
  count                 = 2
  publicIP              = true
  key_name              = "nginx-key"
  user_data             = file("./ansible.sh")
  ec2-tags              = {
    Name                = "Public-Instance-${format("%02d", count.index + 1)}"
    Owner               = local.owner
    Purpose             = "bestion_host"
  }
  securitygroupIds      = [module.securityGroup.securityGroupID]
  subnetId              = local.all_public_subnetsId[count.index]
}

