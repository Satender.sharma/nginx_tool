variable "vpc_cidr" {
  type    = string
  default = "10.10.0.0/20"
}

variable "cidr_public" {
  type = map(string)
  default = {
    ap-southeast-2a = "10.10.0.64/26"
    ap-southeast-2b = "10.10.0.128/26"
  }
}

