resource "aws_vpc" "vpc" {
  cidr_block = var.cidr
  tags       = var.tags
  enable_dns_hostnames = true
}
