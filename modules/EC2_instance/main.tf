
resource "aws_instance" "nginx" {
  ami                           = "ami-09a5c873bc79530d9"
  instance_type                 = "t2.micro"
  tags                          = var.ec2-tags
  user_data                       = var.user_data 
  key_name                      = var.key_name
  vpc_security_group_ids        = var.securitygroupIds
  subnet_id                     = var.subnetId
  associate_public_ip_address   = var.publicIP
}
