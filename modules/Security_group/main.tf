# resource "aws_security_group" "SG" {
#   name        = "securityGroup"
#   vpc_id      = var.vpcId
#   tags        = var.tags
# }

# resource "aws_security_group_rule" "egress" {
#   type              = "egress"
#   from_port         = 0
#   to_port           = 0
#   protocol          = "-1"
#   cidr_blocks       = ["0.0.0.0/0"]
#   security_group_id = aws_security_group.SG.id
# }

resource "aws_security_group" "public-sg" {
  name        = "public traffic"
  vpc_id      = var.vpcId
  tags        = var.tags
  dynamic "ingress" {
    iterator          = port
    for_each          = var.ingressRoles
    content {
    from_port         = port.value
    to_port           = port.value
    protocol          = "TCP"
    cidr_blocks       = ["0.0.0.0/0"]
    }
     
  }

  dynamic "egress" {
    iterator = port
    for_each = var.egressRoles
    content {
    from_port = port.value
    to_port = port.value
    protocol= "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

