variable "cidr" {
  type        = string
  description = "CIDR block for nginx vpc"
  default     = ""
}
variable "tags" {
  default     = {}
  description = "tags for kafka vpc"
  type        = map(string)
}
